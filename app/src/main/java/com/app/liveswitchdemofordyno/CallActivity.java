package com.app.liveswitchdemofordyno;


import android.Manifest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fm.liveswitch.EncodingInfo;
import fm.liveswitch.IAction0;
import fm.liveswitch.IAction1;
import fm.liveswitch.LocalMedia;
import fm.liveswitch.Log;
import fm.liveswitch.VideoEncodingConfig;
import fm.liveswitch.chat.App;
import fm.liveswitch.chat.PagerAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fm.liveswitch.IAction1;
import fm.liveswitch.LocalMedia;
import fm.liveswitch.Log;
import fm.liveswitch.chat.App;
import fm.liveswitch.chat.PagerAdapter;

public class CallActivity extends AppCompatActivity implements OnReceivedTextListener, View.OnTouchListener {

    private static boolean localMediaStarted = false;
    private boolean videoReady = false;
    private boolean textReady = false;
    private App app;
    private final String TAG = "ChatActivity ";

    private ArrayList<Integer> sendEncodings;
    private ArrayList<Integer> recvEncodings;
    private String prefix = "Bitrate: ";
    public static RelativeLayout container;


    private FrameLayout layout;

    private String current_id;

    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        app = App.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (!app.getIsScreenShareEnabled()) {
                    app.useNextVideoDevice();
                }
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

        });

        layout = (FrameLayout) findViewById(R.id.layout);
        app.videoChatFragmentLayout = this;


        // Preserve a static container across
        // activity destruction/recreation.
        RelativeLayout c = (RelativeLayout) findViewById(R.id.container);
        if (container == null) {
            container = c;

            Toast.makeText(this, "Double-tap to switch camera.", Toast.LENGTH_SHORT).show();
        }
        layout.removeView(c);

        container.setOnTouchListener(this);
        boolean isVideoCall = false;
        if (getIntent() != null)
            isVideoCall = getIntent().getBooleanExtra("isVideoCall", false);
        app.isCall = true;
        app.setAudioOnly(!isVideoCall);

        app.joinAsync(this);


        onVideoReady();

    }

    @Override
    public void onBackPressed() {
        stop();
    }

    public void onNewMessage() {

    }

    public void onVideoReady() {
        videoReady = true;

        if (videoReady) {
            start();
        }
    }

    public void onTextReady() {
        textReady = true;

        if (videoReady && textReady) {
            start();
        }

    }

    private void start() {
        if (!localMediaStarted) {


            app.videoChatFragmentLayout = this;

            IAction0 startFn = () -> {
                app.startLocalMedia(this).then(o -> {
                    return app.joinAsync(this);
                }, e -> {
                    Log.error("Could not start local media", e);
                    alert(e.getMessage());
                });
            };

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                List<String> requiredPermissions = new ArrayList<>(3);

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(Manifest.permission.RECORD_AUDIO);
                }
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(Manifest.permission.CAMERA);
                }
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(Manifest.permission.READ_PHONE_STATE);
                }

                if (requiredPermissions.size() == 0) {
                    startFn.invoke();
                } else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO) || shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                            || shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                        Toast.makeText(this, "Access to camera, microphone, storage, and phone call state is required", Toast.LENGTH_SHORT).show();
                    }

                    requestPermissions(requiredPermissions.toArray(new String[0]), 1);
                }
            } else {
                startFn.invoke();
            }
        }
        localMediaStarted = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            // stream api not used here bc not supported under api 24

            boolean permissionsGranted = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                }
            }

            if (permissionsGranted) {
                app.videoChatFragmentLayout = this;

                app.startLocalMedia(this).then(o -> {
                    return app.joinAsync(this);
                }, e -> {
                    Log.error("Could not start local media", e);
                    alert(e.getMessage());
                });
                app.joinAsync(this);
            } else {
                Toast.makeText(this, "Cannot connect without access to camera and microphone", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Log.debug("permission to " + permissions[i] + " not granted");
                    }
                }
                stop();
            }
        } else {
            Toast.makeText(this, "Unknown permission requested", Toast.LENGTH_SHORT).show();
        }
    }

    private void stop() {
        if (localMediaStarted) {
            //Future<Object> promise =
            app.leaveAsync().then(new IAction1<Object>() {
                @Override
                public void invoke(Object o) {
                    stopLocalMediaAndFinish();
                }
            }).fail(new IAction1<Exception>() {
                @Override
                public void invoke(Exception e) {
                    Log.error("Could not leave conference", e);
                    alert(e.getMessage());
                }
            });
        } else {
            finish();
        }
        localMediaStarted = false;
    }

    private void stopLocalMediaAndFinish() {
        app.stopLocalMedia().then(new IAction1<LocalMedia>() {
            @Override
            public void invoke(LocalMedia o) {
                finish();
            }
        }).fail(new IAction1<Exception>() {
            @Override
            public void invoke(Exception e) {
                Log.error("Could not stop local media", e);
                alert(e.getMessage());
            }
        });
    }

    public void alert(String format, Object... args) {
        final String text = String.format(format, args);
        final AppCompatActivity activity = this;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setMessage(text);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });
    }

/*    private void UpdateBadge(boolean visible) {
        View view = tabLayout.getTabAt(PagerAdapter.TextTabIndex).getCustomView();
        TextView textView = (TextView) view.findViewById(R.id.badge);
        textView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);

        if (visible) {
            int newNumber = Integer.parseInt(textView.getText().toString()) + 1;
            textView.setText(Integer.toString(newNumber));
        } else {
            textView.setText("0");
        }
    }*/

    @Override
    public void onReceivedText(String name, String message) {
        runOnUiThread(() -> Toast.makeText(this, name + " -> " + message, Toast.LENGTH_LONG).show());

        Log.debug(TAG + "Received Text -> " + name + ": Message -> " + message);
    }

    @Override
    public void onPeerJoined(String name) {
        Log.debug(TAG + "Peer Joined ->" + name);
    }

    @Override
    public void onPeerLeft(String name) {
        Log.debug(TAG + "Peer left ->" + name);
    }

    @Override
    public void onClientRegistered() {
        Log.debug(TAG + "Client Registered");
    }

    @Override
    public void onClientUnregistered() {
        Log.debug(TAG + "Client Unregistered");
    }

    //////////////////////////////////////////////
    public void registerLocalContextMenu(View view, VideoEncodingConfig[] encodings) {
        String id = view.getContentDescription().toString();
        sendEncodings = new ArrayList<>();
        app.contextMenuItemFlag.put("MuteAudio", false);
        app.contextMenuItemFlag.put("MuteVideo", false);
        app.contextMenuItemFlag.put("DisableAudio", false);
        app.contextMenuItemFlag.put("DisableVideo", false);
        if (encodings != null && encodings.length > 1) {
            for (int i = 0; i < encodings.length; i++) {
                int bitrate = getBitrate(encodings[i].toString());
                sendEncodings.add(bitrate);
                app.contextMenuItemFlag.put(id + prefix + bitrate, true);
            }
        }
        registerForContextMenu(view);
    }

    public void registerRemoteContextMenu(View view, EncodingInfo[] encodings) {
        String id = view.getContentDescription().toString();
        recvEncodings = new ArrayList<>();
        app.contextMenuItemFlag.put(id + "DisableAudio", false);
        app.contextMenuItemFlag.put(id + "DisableVideo", false);
        if (encodings != null && encodings.length > 1) {
            for (int i = 0; i < encodings.length; i++) {
                int bitrate = getBitrate(encodings[i].toString());
                recvEncodings.add(bitrate);
                app.contextMenuItemFlag.put(id + prefix + bitrate, i == 0);
            }
        }
        registerForContextMenu(view);
    }


    private int getBitrate(String encoding) {
        String[] str = encoding.split(",");
        for (int i = 0; i < str.length; i++) {
            if (str[i].contains("Bitrate")) {
                return Integer.parseInt(str[i].split(":")[1].trim());
            }
        }
        return 0;
    }

    public void updateRecvEncodingFlag(String id, int bitrate) {
        for (Map.Entry flag : app.contextMenuItemFlag.entrySet()) {
            String key = (String) flag.getKey();
            if (key.contains(id + prefix)) {
                if (key.equals(id + prefix + bitrate)) {
                    app.contextMenuItemFlag.put(key, true);
                } else {
                    app.contextMenuItemFlag.put(key, false);
                }
            }
        }
    }

    public void onPause() {
        // Android requires us to pause the local
        // video feed when pausing the activity.
        // Not doing this can cause unexpected side
        // effects and crashes.
        app.pauseLocalVideo().waitForResult();

        // Remove the static container from the current layout.
        if (container != null) {
            layout.removeView(container);
        }

        super.onPause();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        // Handle the double-tap event.
        boolean gestureResponse = false;
        if (gestureDetector != null) {
            gestureResponse = gestureDetector.onTouchEvent(motionEvent);
        }
        if (!gestureResponse) {
            return view.onTouchEvent(motionEvent);
        }
        return gestureResponse;
    }

    public void onResume() {
        super.onResume();

        // Add the static container to the current layout.
        if (container != null) {
            layout.addView(container);
        }

        // Resume the local video feed.
        app.resumeLocalVideo().waitForResult();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        String id = v.getContentDescription().toString();
        current_id = id;
        int index = 0;
        if (id.equals("localView")) {
            menu.setHeaderTitle("Local");
            MenuItem muteAudio = menu.add(0, 0, 0, "Mute Audio");
            MenuItem muteVideo = menu.add(0, 1, 0, "Mute Video");
            MenuItem disableAudio = menu.add(0, 2, 0, "Disable Audio");
            MenuItem disableVideo = menu.add(0, 3, 0, "Disable Video");
            menu.setGroupCheckable(0, true, false);
            muteAudio.setChecked(app.contextMenuItemFlag.get("MuteAudio"));
            muteVideo.setChecked(app.contextMenuItemFlag.get("MuteVideo"));
            disableAudio.setChecked(app.contextMenuItemFlag.get("DisableAudio"));
            disableVideo.setChecked(app.contextMenuItemFlag.get("DisableVideo"));
            if (app.getEnableSimulcast()) {
                if (sendEncodings != null) {
                    Collections.sort(sendEncodings);
                    Collections.reverse(sendEncodings);
                    SubMenu encodings = menu.addSubMenu(0, 2, 0, "Video Encoding");
                    for (int bitrate : sendEncodings) {
                        MenuItem item = encodings.add(1, index, 0, prefix + bitrate);
                        item.setChecked(app.contextMenuItemFlag.get(id + prefix + bitrate));
                        index++;
                    }
                    encodings.setGroupCheckable(1, true, false);
                }
            }
        } else {
            menu.setHeaderTitle("Remote");
            MenuItem disableAudio = menu.add(2, 0, 0, "Disable Audio");
            MenuItem disableVideo = menu.add(2, 1, 0, "Disable Video");
            menu.setGroupCheckable(2, true, false);
            disableAudio.setChecked(app.contextMenuItemFlag.get(id + "DisableAudio"));
            disableVideo.setChecked(app.contextMenuItemFlag.get(id + "DisableVideo"));
            if (app.getEnableSimulcast()) {
                // Refresh the recvEncoding List in case of each remote media has different encoding
                if (recvEncodings != null) {
                    recvEncodings.clear();
                    for (Map.Entry flag : app.contextMenuItemFlag.entrySet()) {
                        String key = (String) flag.getKey();
                        if (key.contains(id + prefix)) {
                            recvEncodings.add(Integer.parseInt(key.split(":")[1].trim()));
                        }
                    }
                    Collections.sort(recvEncodings);
                    Collections.reverse(recvEncodings);
                    SubMenu encodings = menu.addSubMenu(2, 2, 0, "Video Encoding");
                    for (int bitrate : recvEncodings) {
                        MenuItem item = encodings.add(3, index, 0, prefix + bitrate);
                        item.setChecked(app.contextMenuItemFlag.get(id + prefix + bitrate));
                        index++;
                    }
                    encodings.setGroupCheckable(3, true, false);
                }
            }
        }

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        String id = current_id;
        int itemId = item.getItemId();
        if (item.getGroupId() == 0) {
            switch (item.getItemId()) {
                case 0:
                    app.toggleMuteAudio();
                    break;
                case 1:
                    app.toggleMuteVideo();
                    break;
                case 2:
                    app.toggleLocalDisableAudio();
                    break;
                case 3:
                    app.toggleLocalDisableVideo();
                    break;
            }
        }
        if (item.getGroupId() == 2) {
            switch (item.getItemId()) {
                case 0:
                    app.toggleRemoteDisableAudio(id);
                    break;
                case 1:
                    app.toggleRemoteDisableVideo(id);
                    break;
            }
        }
        if (item.getGroupId() == 1) {
            //toggleSendEncoding on local media
            app.changeSendEncodings(itemId);
            app.contextMenuItemFlag.put(id + prefix + sendEncodings.get(itemId), !app.contextMenuItemFlag.get(id + prefix + sendEncodings.get(itemId)));
        }
        if (item.getGroupId() == 3) {
            //toggleRecvEncoding on selected remote media
            app.changeReceiveEncodings(id, itemId);
            updateRecvEncodingFlag(id, recvEncodings.get(itemId));
        }
        return true;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.videoChatFragmentLayout = null;
    }


    ////////////////////////////////////////////////

}