package com.app.liveswitchdemofordyno;

import android.Manifest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import fm.liveswitch.IAction0;
import fm.liveswitch.IAction1;
import fm.liveswitch.LocalMedia;
import fm.liveswitch.Log;
import fm.liveswitch.chat.App;
import fm.liveswitch.chat.PagerAdapter;


public class ChatActivity extends AppCompatActivity implements OnReceivedTextListener {

    private static boolean localMediaStarted = false;
    private boolean videoReady = false;
    private boolean textReady = false;
    private App app;
    private RecyclerAdapter adapter;

    private RecyclerView recyclerView;

    private ArrayList<String> messages = new ArrayList<>();

    private final String TAG = "ChatActivity ";

    private Button sendButton;
    private EditText messageText;
    private ImageView videoImageView;
    private ImageView audioImageView;
    private String channelId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        app = App.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        channelId = getIntent().getStringExtra("channelId");
        recyclerView = findViewById(R.id.recyclerView);
        sendButton = findViewById(R.id.sendButton);
        messageText = findViewById(R.id.messageText);
        videoImageView = findViewById(R.id.videoCallButton);
        audioImageView = findViewById(R.id.audioCallButton);
        adapter = new RecyclerAdapter(messages);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


        sendButton.setOnClickListener((view) -> {
            String msg = messageText.getText().toString().trim();
            messageText.setText("");

            if (msg.length() > 0) {
                messages.add(" >" + msg);
                app.writeLine(msg);
                adapter.notifyDataSetChanged();
            }
        });
        videoImageView.setOnClickListener((view) -> {
            app.writeLine("initiateVideoCall");
            openCallActivity(true);
        });

        audioImageView.setOnClickListener((view) -> {
            app.writeLine("initiateAudioCall");
            openCallActivity(false);
        });

        app.joinAsync(this);

    }

    private void openCallActivity(boolean isVideoCall) {
        Intent callIntent = new Intent(this, CallActivity.class);
        callIntent.putExtra("isVideoCall", isVideoCall);
        startActivity(callIntent);
    }

    @Override
    public void onBackPressed() {
        stop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            // stream api not used here bc not supported under api 24

            boolean permissionsGranted = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                }
            }

            if (permissionsGranted) {
                app.joinAsync(this);
            } else {
                Toast.makeText(this, "Cannot connect without access to camera and microphone", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Log.debug("permission to " + permissions[i] + " not granted");
                    }
                }
                stop();
            }
        } else {
            Toast.makeText(this, "Unknown permission requested", Toast.LENGTH_SHORT).show();
        }
    }

    private void stop() {
        // if (localMediaStarted) {
        //Future<Object> promise =
        app.leaveAsync().then(new IAction1<Object>() {
            @Override
            public void invoke(Object o) {
                stopLocalMediaAndFinish();
            }
        }).fail(new IAction1<Exception>() {
            @Override
            public void invoke(Exception e) {
                Log.error("Could not leave conference", e);
                alert(e.getMessage());
            }
        });
        //   } else {
        finish();
        // }
        localMediaStarted = false;
    }

    private void stopLocalMediaAndFinish() {
        app.stopLocalMedia().then(new IAction1<LocalMedia>() {
            @Override
            public void invoke(LocalMedia o) {
                finish();
            }
        }).fail(new IAction1<Exception>() {
            @Override
            public void invoke(Exception e) {
                Log.error("Could not stop local media", e);
                alert(e.getMessage());
            }
        });
    }

    public void alert(String format, Object... args) {
        final String text = String.format(format, args);
        final AppCompatActivity activity = this;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setMessage(text);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            }
        });
    }

    @Override
    public void onReceivedText(String name, String message) {
        if (message.equals("initiateVideoCall")) {
            //Todo: Show Incoming Audio Call Screen
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, name + " -> " + message, Toast.LENGTH_LONG).show());
            Log.debug(TAG + "Incoming Video Call -> " + name + ": Message -> " + message);
        } else if (message.equals("initiateAudioCall")) {
            //Todo: Show Incoming Audio Call Screen
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, name + " -> " + message, Toast.LENGTH_LONG).show());
            Log.debug(TAG + "Incoming Audio Call -> " + name + ": Message -> " + message);
        } else {
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, name + " -> " + message, Toast.LENGTH_LONG).show());

            Log.debug(TAG + "Received Text -> " + name + ": Message -> " + message);

            messages.add(name + ">" + message);
            adapter.notifyDataSetChanged();
            recyclerView.smoothScrollToPosition(messages.size() - 1);
        }
    }

    @Override
    public void onPeerJoined(String name) {
        Log.debug(TAG + "Peer Joinned ->" + name);
    }

    @Override
    public void onPeerLeft(String name) {
        Log.debug(TAG + "Peer left ->" + name);
    }

    @Override
    public void onClientRegistered() {
        Log.debug(TAG + "Client Registered");
    }

    @Override
    public void onClientUnregistered() {
        Log.debug(TAG + "Client Unregistered");
    }
}