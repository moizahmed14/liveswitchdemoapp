package com.app.liveswitchdemofordyno

data class Friend(
        val channel_id: String,
        val friend_email: String,
        val friend_name: String,
        val friend_type: String,
        val since: String
)