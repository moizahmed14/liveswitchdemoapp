package com.app.liveswitchdemofordyno

data class FriendListModel(
    val friendList: List<Friend>
)