package com.app.liveswitchdemofordyno;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> messages;

    public RecyclerAdapter(ArrayList<String> messages) {
        this.messages = messages;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        if (viewType == R.layout.item_received_message || viewType == R.layout.item_sent_message) {
            return new ConversationViewHolder(view, viewType == R.layout.item_sent_message);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ConversationViewHolder) {
            ConversationViewHolder vHolder = (ConversationViewHolder) holder;
            vHolder.messageTV.setText(messages.get(position).split(">")[1]);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ConversationViewHolder extends RecyclerView.ViewHolder {
        TextView messageTV;
        boolean isAtMySide;

        public ConversationViewHolder(@NonNull View itemView, boolean isAtMySide) {
            super(itemView);
            this.isAtMySide = isAtMySide;
            messageTV = (TextView) itemView.findViewById(R.id.message);
        }
    }

    @Override
    public int getItemViewType(int position) {

        int cellType = 0;
        if (isMyMessage(messages.get(position).split(">")[0])) {
            cellType = R.layout.item_sent_message;
        } else {
            cellType = R.layout.item_received_message;
        }

        return cellType;
    }

    private boolean isMyMessage(String mail) {
        if (mail != null && mail.equals("Dyno")) {
            return true;
        }
        return false;
    }
}
