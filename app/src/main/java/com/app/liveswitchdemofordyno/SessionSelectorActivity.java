package com.app.liveswitchdemofordyno;


import android.content.DialogInterface;
import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import fm.liveswitch.Channel;
import fm.liveswitch.Future;
import fm.liveswitch.IAction1;
import fm.liveswitch.Log;
import fm.liveswitch.chat.App;

public class SessionSelectorActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, OnReceivedTextListener {

    private static String[] Names = {
            "Dyno"
    };

    private Button joinButton;
    private EditText channelText;
    private EditText nameText;
    private Spinner modeSpinner;
    private CheckBox audioOnlyCheckBox;
    private CheckBox receiveOnlyCheckBox;
    private CheckBox simulcastCheckBox;
    private CheckBox screenShareCheckBox;
    private App app;

    ArrayList<String> friends = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_session_selector);
        getFriendsChannelIds();
        app = App.getInstance(this);

        nameText = (EditText) findViewById(R.id.userNameInput);
        channelText = (EditText) findViewById(R.id.channelInput);
        joinButton = (Button) findViewById(R.id.joinButton);

        audioOnlyCheckBox = (CheckBox) findViewById(R.id.audioOnlyInput);
        receiveOnlyCheckBox = (CheckBox) findViewById(R.id.receiveOnlyInput);
        simulcastCheckBox = (CheckBox) findViewById(R.id.simulcastInput);
        screenShareCheckBox = (CheckBox) findViewById(R.id.screenShareInput);

        // Populate our mode spinner with App.Mode values and default app.mode to the first.
        modeSpinner = (Spinner) findViewById(R.id.connectionModeInput);
        modeSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, App.Modes.values()));
        modeSpinner.setOnItemSelectedListener(this);
        app.setMode(App.Modes.values()[0]);


        try {

            // Create a random 6 digit number for the new channel ID.
            channelText.setText("123456");
            //  channelText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});

            nameText.setText(Names[new fm.liveswitch.Randomizer().next(Names.length)]);
            nameText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});

            joinButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    switchToVideoChat(channelText.getText().toString(), nameText.getText().toString());
                }
            });

            audioOnlyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    app.setAudioOnly(b);
                    if (b) {
                        simulcastCheckBox.setChecked(false);
                    }
                    simulcastCheckBox.setEnabled(!b);
                }
            });

            receiveOnlyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    app.setReceiveOnly(b);
                    if (b) {
                        simulcastCheckBox.setChecked(false);
                    }
                    simulcastCheckBox.setEnabled(!b);
                }
            });

            screenShareCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    app.setEnableScreenShare(b);
                }
            });

            simulcastCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    app.setEnableSimulcast(b);
                    if (b) {
                        audioOnlyCheckBox.setChecked(false);
                        receiveOnlyCheckBox.setChecked(false);
                    }
                    audioOnlyCheckBox.setEnabled(!b);
                    receiveOnlyCheckBox.setEnabled(!b);
                }
            });

            ((TextView) findViewById(R.id.phoneText)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:18883796686"));
                    startActivity(tel);
                }
            });

            ((TextView) findViewById(R.id.emailText)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:info@frozenmountain.com");
                    intent.setData(data);
                    startActivity(intent);
                }
            });

            ((ImageView) findViewById(R.id.facebookIcon)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("https://www.facebook.com/frozenmountain");
                    intent.setData(data);
                    startActivity(intent);
                }
            });

            ((ImageView) findViewById(R.id.twitterIcon)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("https://twitter.com/frozenmountain");
                    intent.setData(data);
                    startActivity(intent);
                }
            });

            ((ImageView) findViewById(R.id.linkedinIcon)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("https://www.linkedin.com/company/frozen-mountain-software");
                    intent.setData(data);
                    startActivity(intent);
                }
            });
         /*   ArrayList<Future<Channel[]>> channels =
                    app.joinAsync(friends, this);

            Log.e("Channels", channels.toString());*/

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // Handle mode selection
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        app.setMode((App.Modes) parent.getItemAtPosition(pos));
        boolean modeIsPeer = app.getMode() == App.Modes.Peer;
        if (modeIsPeer) {
            simulcastCheckBox.setChecked(false);
        }
        simulcastCheckBox.setEnabled(!modeIsPeer);
    }


    public void onNothingSelected(AdapterView<?> parent) {
        // We don't care
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 42 && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            if (data == null) {
                alert("Must allow screen sharing before the chat can start.");
            } else {
                MediaProjectionManager manager = (MediaProjectionManager) this.getSystemService(MEDIA_PROJECTION_SERVICE);
                app.setMediaProjection(manager.getMediaProjection(resultCode, data));
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
            }
        }
    }


    private void switchToVideoChat(String channelId, String name) {
        //  if (channelId.length() == 6) {
        if (name.length() > 0) {
            app.setChannelId(channelId);
            app.setUserName(name);
            app.setEnableScreenShare(screenShareCheckBox.isChecked());

            if (screenShareCheckBox.isChecked() && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                MediaProjectionManager manager = (MediaProjectionManager) this.getSystemService(MEDIA_PROJECTION_SERVICE);
                Intent screenCaptureIntent = manager.createScreenCaptureIntent();

                this.startActivityForResult(screenCaptureIntent, 42);
            } else {
                // Show the video chat.
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
            }
        } else {
            alert("Must have a name.");
        }
    } /*else {
            alert("Channel ID must be 6 digits long.");
        }*/
    //}

    public void alert(String format, Object... args) {
        final String text = String.format(format, args);
        final AppCompatActivity self = this;
        self.runOnUiThread(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(self);
                    alert.setMessage(text);
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.show();
                }
            }
        });
    }

    private void getFriendsChannelIds() {

        friends.add("123456");
        friends.add("236545");
        /*friends.add("81560906-5f38-46d9-b898-a9e6438db945");
        friends.add("040edbe0-19f9-4982-84a3-b4817d9faff9");*/

        friends.add("9b58057d-69d2-4738-9142-cba9f9009b61");
        friends.add("e593cb2f-bb97-43d2-a503-0bd0706cd269");
        friends.add("81560906-5f38-46d9-b898-a9e6438db945");
        friends.add("040edbe0-19f9-4982-84a3-b4817d9faff9");
    }

    @Override
    public void onReceivedText(String name, String message) {
        runOnUiThread(() -> Toast.makeText(this, message, Toast.LENGTH_LONG).show());
    }

    @Override
    public void onPeerJoined(String name) {

    }

    @Override
    public void onPeerLeft(String name) {

    }

    @Override
    public void onClientRegistered() {

    }

    @Override
    public void onClientUnregistered() {

    }

  /*  @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    private void stop() {

        //Future<Object> promise =
        app.leaveAsync().then(new IAction1<Object>() {
            @Override
            public void invoke(Object o) {

            }
        }).fail(new IAction1<Exception>() {
            @Override
            public void invoke(Exception e) {
                Log.error("Could not leave conference", e);
                alert(e.getMessage());
            }
        });

    }*/
}
