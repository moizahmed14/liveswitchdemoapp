package com.app.liveswitchdemofordyno;

public interface OnReceivedTextListener {

    void onReceivedText(String name, String message);

    void onPeerJoined(String name);

    void onPeerLeft(String name);

    void onClientRegistered();

    void onClientUnregistered();

}
