package fm.liveswitch.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.app.liveswitchdemofordyno.R;
import com.app.liveswitchdemofordyno.TextChatFragment;
import com.app.liveswitchdemofordyno.VideoChatFragment;


public class PagerAdapter extends FragmentPagerAdapter {
    Context context;
    
    public static final int VideoTabIndex = 0;
    public static final int TextTabIndex = 1;
    
    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
    }
    
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case VideoTabIndex:
                return new VideoChatFragment();
            case TextTabIndex:
                return new TextChatFragment();
            default:
                return null;
        }
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case VideoTabIndex:
                return "Video";
            case TextTabIndex:
                return "Text";
            default:
                return "Unknown";
        }
    }
    
    @Override
    public int getCount() {
        return 2;
    }
    
    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.tab, null);
        TextView header = (TextView)v.findViewById(R.id.header);
        header.setText(getPageTitle(position));
        
        TextView badge = (TextView)v.findViewById(R.id.badge);
        badge.setVisibility(View.INVISIBLE);
        
        return v;
    }
}
